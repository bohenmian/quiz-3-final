CREATE DATABASE IF NOT EXISTS `shopping` DEFAULT CHAR SET utf8;

USE `shopping`;

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`
(
    `id`         BIGINT(15) AUTO_INCREMENT NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8);

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`
(
    `id`       BIGINT(15) AUTO_INCREMENT NOT NULL,
    `name`     VARCHAR(25)               NOT NULL,
    `price`    DECIMAL                   NOT NULL,
    `unit`     VARCHAR(25)               NOT NULL,
    `url`      VARCHAR(128)              NOT NULL,
    `order_id` BIGINT(15)                NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (order_id) REFERENCES orders (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


