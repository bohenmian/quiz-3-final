import {createProduct} from "../utils/data";

export const createProductAction = (name, price, unit, url) => dispatch => {
    createProduct('http://localhost:8080/api/products', name, price, unit, url)
        .then(() => {
            dispatch(getProducts());
            window.location.href = '/';
        })
};
export const getProducts = () => dispatch => {
    fetch('http://localhost:8080/api/products')
        .then(response => response.json())
        .then(response => {
            dispatch({
                type: 'GET_PRODUCTS',
                products: response
            })
        })
};
export const orderProduct = (productId) => {
    fetch('http://localhost:8080/api/orders/', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({productId: productId})
    }).then();
};


export const getOrders = () => (dispatch) => {
    fetch('http://localhost:8080/api/orders')
        .then(response => response.json())
        .then(data => {
            dispatch({
                type: 'GET_ORDERS',
                orders: data
            })
        })
};

export const deleteOrder = (productId) => dispatch => {
    fetch('http://localhost:8080/api/orders/' + encodeURIComponent(productId), {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        if (response.status === 200) {
            dispatch(getOrders())
        }
    }).catch(Error);
};
