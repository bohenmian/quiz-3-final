export const HANDLE_NAME = "HANDLE_NAME";

export const handleName = (name) => (dispatch) => {
    dispatch({
        type: HANDLE_NAME,
        newProductName: name
    });
};
