export const HANDLE_URL= "HANDLE_URL";

export const handleUrl= (url) => (dispatch) => {
    dispatch({
        type: HANDLE_URL,
        newProductUrl: url
    })
};