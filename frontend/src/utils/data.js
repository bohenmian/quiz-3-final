const createProduct = (uri, name, price, unit, url) =>
    fetch(uri, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({name: name, price: price, unit: unit, url: url})
        }
    );
export {
    createProduct
};
