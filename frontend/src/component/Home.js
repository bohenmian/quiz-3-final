import * as React from "react";
import {connect} from "react-redux";
import {getProducts} from "../action/productActions";
import Item from "./Item";
import {bindActionCreators} from "redux";

class Home extends React.Component {

    componentDidMount() {
        this.props.getProducts();
    }

    render() {
        const data = this.props.products;
        return (
            <div className="home">
                {data.map(item => <Item id={item.id} name={item.name} price={item.price} unit={item.unit} url={item.url}/>)}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    products: state.product.products
});
const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
