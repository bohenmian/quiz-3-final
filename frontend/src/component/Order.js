import * as React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {deleteOrder} from "../action/productActions";

class Order extends React.Component {

    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }
    render() {
        return (
            <div>
                <tr>
                    <td>{this.props.name}</td>
                    <td>{this.props.price}</td>
                    <td>{this.props.quantity}</td>
                    <td>{this.props.unit}</td>
                    <td>
                        <button onClick={this.handleDelete} className="delete-btn">删除</button>
                    </td>
                </tr>
            </div>
        );
    }

    handleDelete() {
        this.props.deleteOrder(this.props.id);
    }
}

const mapStateToProps = state => ({
    orders: state.product.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
    deleteOrder
}, dispatch);



export default connect(mapStateToProps, mapDispatchToProps)(Order);
