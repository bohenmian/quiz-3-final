import React, {Component} from 'react';
import {connect} from "react-redux";
import {orderProduct} from "../action/productActions";
import {bindActionCreators} from "redux";

class Item extends Component {

    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
    }

    render() {
        return (
            <div className="item">
                <img src={this.props.url} alt={"图片无法显示"}/>
                <h4>{this.props.name}</h4>
                <span>单价：{this.props.price}元/{this.props.unit}</span><br/>
                <button onClick={this.addToCart}>+</button>
            </div>
        );
    }
    addToCart() {
        this.props.orderProduct(this.props.id);
    }
}

const mapStateToProps = state => ({
    orders: state.product.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
    orderProduct
}, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(Item);


