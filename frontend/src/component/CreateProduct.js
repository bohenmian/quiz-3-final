import React, {Component} from 'react';
import {connect} from "react-redux";
import {handleName} from "../action/handleName";
import {handlePrice} from "../action/handlePrice";
import {handleUnit} from "../action/handleUnit";
import {handleUrl} from "../action/handleUrl";
import {createProductAction} from "../action/productActions";

class CreateProduct extends Component {

    constructor(props, context) {
        super(props, context);
        this.handleName = this.handleName.bind(this);
        this.handlePrice = this.handlePrice.bind(this);
        this.handleUnit = this.handleUnit.bind(this);
        this.handleUrl = this.handleUrl.bind(this);
        this.submit = this.submit.bind(this);
    }

    handleName(event) {
        this.props.handleName(event.target.value);
    }

    handlePrice(event) {
        this.props.handlePrice(event.target.value);
    }

    handleUnit(event) {
        this.props.handleUnit(event.target.value);
    }

    handleUrl(event) {
        this.props.handleUrl(event.target.value);
    }

    submit() {
        this.props.createProduct(
            this.props.newProductName, this.props.newProductPrice,
            this.props.newProductUnit, this.props.newProductUrl
        );
    }

    render() {
        return (
            <div className="create-main">
                <h1>添加商品</h1>
                <span className="properties"><span>*</span>名称:<br/>
                <input placeholder="名称" onChange={this.handleName}/></span><br/>
                <span className="properties"><span>*</span>价格:<br/>
                <input placeholder="价格" onChange={this.handlePrice}/></span><br/>
                <span className="properties"><span>*</span>单位:<br/>
                <input placeholder="单位" onChange={this.handleUnit}/></span><br/>
                <span className="properties"><span>*</span>图片:<br/>
                <input placeholder="图片" onChange={this.handleUrl}/></span><br/>
                {this.props.newProductName && this.props.newProductPrice && this.props.newProductUnit && this.props.newProductUrl ?
                    <button onClick={this.submit} className="submit">提交</button> :
                    <button disabled>提交</button>}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    newProductName: state.product.newProductName,
    newProductPrice: state.product.newProductPrice,
    newProductUnit: state.product.newProductUnit,
    newProductUrl: state.product.newProductUrl
});

const mapDispatchToProps = dispatch => ({
    createProduct: (name, price, unit, url) => dispatch(createProductAction(name, price, unit, url)),
    handleName: (name) => dispatch(handleName(name)),
    handlePrice: (price) => dispatch(handlePrice(price)),
    handleUnit: (unit) => dispatch(handleUnit(unit)),
    handleUrl: (url) => dispatch(handleUrl(url))
});


export default connect(mapStateToProps, mapDispatchToProps)(CreateProduct);
