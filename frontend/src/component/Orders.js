import * as React from "react";
import {bindActionCreators} from "redux";
import {getOrders} from "../action/productActions";
import {connect} from "react-redux";
import Order from "./Order";

class Orders extends React.Component {

    componentDidMount() {
        this.props.getOrders();
        console.log(1);
    }

    render() {
        const orders = this.props.orders || [];
        return (
            <div>
                {orders.length === 0 ? "暂无订单，返回商城继续购买" :
                    <table>
                        <tr>
                            <th>名字</th>
                            <th>单价</th>
                            <th>数量</th>
                            <th>单位</th>
                            <th>操作</th>
                        </tr>
                        {orders.map(item => <Order id={item.id} name={item.name} price={item.price}
                                                   quantity={item.quantity} unit={item.unit}/>)}
                    </table>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    orders: state.product.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getOrders
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Orders);
