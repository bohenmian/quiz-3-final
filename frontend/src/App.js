import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import CreateProduct from "./component/CreateProduct";
import Home from "./component/Home";
import Orders from "./component/Orders";

class App extends Component {
    render() {
        return (
            <div>
                <Router>
                    <header>
                        <Link to='/'>商城</Link>
                        <Link to='/orders'>订单</Link>
                        <Link to='/products/create'>添加商品</Link>
                    </header>
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route path ='/orders' component={Orders}/>
                        <Route path='/products/create' component={CreateProduct}/>
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App;
