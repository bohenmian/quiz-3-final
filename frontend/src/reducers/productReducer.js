import {HANDLE_URL} from "../action/handleUrl";
import {HANDLE_UNIT} from "../action/handleUnit";
import {HANDLE_PRICE} from "../action/handlePrice";
import {HANDLE_NAME} from "../action/handleName";

export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_PRODUCTS':
            return {
                ...state,
                products: action.products
            };
        case 'HANDLE_NAME':
            return {
                ...state,
                newProductName: action.newProductName
            };
        case 'HANDLE_PRICE':
            return {
                ...state,
                newProductPrice: action.newProductPrice
            };
        case 'HANDLE_UNIT':
            return {
                ...state,
                newProductUnit: action.newProductUnit
            };
        case 'HANDLE_URL':
            return {
                ...state,
                newProductUrl: action.newProductUrl
            };
        case 'GET_ORDERS':
            return {
                ...state,
                orders: action.orders
            };
        default:
            return state;
    }
};

const initialState = {
    orders: [],
    products: []
};
