package com.twc.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GetOrderResponse {

    private Long id;
    private String name;
    private BigDecimal price;
    private Long quantity;
    private String unit;

}
