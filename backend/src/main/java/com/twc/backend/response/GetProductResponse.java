package com.twc.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class GetProductResponse {

    private Long id;
    private String name;
    private BigDecimal price;
    private String unit;
    private String url;
}
