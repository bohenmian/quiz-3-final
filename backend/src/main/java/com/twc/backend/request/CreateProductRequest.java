package com.twc.backend.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CreateProductRequest {

    @NotNull
    @Size(max = 25)
    private String name;

    @NotNull
    private BigDecimal price;

    @NotNull
    @Size(max = 25)
    private String unit;

    @NotNull
    private String url;

}
