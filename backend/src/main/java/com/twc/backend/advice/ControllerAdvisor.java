package com.twc.backend.advice;

import com.twc.backend.exception.ProductNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor {

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity handlerException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("not found product");
    }


}
