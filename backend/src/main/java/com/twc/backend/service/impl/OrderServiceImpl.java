package com.twc.backend.service.impl;

import com.twc.backend.entity.Order;
import com.twc.backend.entity.Product;
import com.twc.backend.exception.ProductNotFoundException;
import com.twc.backend.repository.OrderRepository;
import com.twc.backend.repository.ProductRepository;
import com.twc.backend.request.CreateOrderRequest;
import com.twc.backend.response.GetOrderResponse;
import com.twc.backend.service.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private ProductRepository productRepository;

    public OrderServiceImpl(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @Override
    @Transactional
    public Order createOrder(CreateOrderRequest request) {
        Product product = productRepository.findById(request.getProductId()).orElseThrow(ProductNotFoundException::new);
        return orderRepository.save(new Order(product));
    }

    @Override
    public List<GetOrderResponse> getOrder() {
        return orderRepository.findAll().stream()
                .collect(Collectors.groupingBy(order -> order.getProduct().getId(), Collectors.counting()))
                .entrySet().stream().map(item -> {
                    Product product = productRepository.findById(item.getKey()).orElseThrow(ProductNotFoundException::new);
                    return new GetOrderResponse(item.getKey(), product.getName(), product.getPrice(), item.getValue(), product.getUnit());
                }).collect(Collectors.toList());

    }

    @Override
    @Transactional
    public void deleteOrder(Long productId) {
        orderRepository.deleteAllByProductId(productId);
    }
}
