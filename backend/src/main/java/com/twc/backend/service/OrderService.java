package com.twc.backend.service;

import com.twc.backend.entity.Order;
import com.twc.backend.request.CreateOrderRequest;
import com.twc.backend.response.GetOrderResponse;

import java.util.List;

public interface OrderService {
    Order createOrder(CreateOrderRequest request);

    List<GetOrderResponse> getOrder();

    void deleteOrder(Long productId);
}
