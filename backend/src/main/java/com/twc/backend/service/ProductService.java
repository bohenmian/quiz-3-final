package com.twc.backend.service;

import com.twc.backend.entity.Product;
import com.twc.backend.request.CreateProductRequest;
import com.twc.backend.response.GetProductResponse;

import java.util.List;

public interface ProductService {

    Product createProduct(CreateProductRequest request);

    List<GetProductResponse> getAllProducts();
}
