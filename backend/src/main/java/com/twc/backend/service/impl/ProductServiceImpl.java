package com.twc.backend.service.impl;


import com.twc.backend.entity.Product;
import com.twc.backend.repository.ProductRepository;
import com.twc.backend.request.CreateProductRequest;
import com.twc.backend.response.GetProductResponse;
import com.twc.backend.service.ProductService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product createProduct(CreateProductRequest request) {
        return productRepository.save(new Product(request.getName(), request.getPrice(), request.getUnit(), request.getUrl()));
    }

    @Override
    public List<GetProductResponse> getAllProducts() {
        return productRepository.findAll(Sort.by(Sort.Direction.ASC, "id")).stream()
                .map(product -> new GetProductResponse(product.getId(), product.getName(), product.getPrice(), product.getUnit(), product.getUrl()))
                .collect(Collectors.toList());
    }
}
