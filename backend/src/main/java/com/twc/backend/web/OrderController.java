package com.twc.backend.web;

import com.twc.backend.request.CreateOrderRequest;
import com.twc.backend.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class OrderController {

    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }


    @PostMapping("/orders")
    public ResponseEntity createOrder(@RequestBody @Valid CreateOrderRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderService.createOrder(request));
    }

    @GetMapping("/orders")
    public ResponseEntity getOrder() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(orderService.getOrder());
    }

    @DeleteMapping("/orders/{productId}")
    public ResponseEntity deleteOrder(@PathVariable Long productId) {
        orderService.deleteOrder(productId);
        return ResponseEntity.status(HttpStatus.OK)
                .build();
    }
}
