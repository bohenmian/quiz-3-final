package com.twc.backend.web;

import com.twc.backend.ApiTestBase;
import com.twc.backend.request.CreateProductRequest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class ProductControllerTest extends ApiTestBase {

    @Test
    void should_return_success_when_create_product() throws Exception {
        getPerform(new CreateProductRequest("可乐", new BigDecimal(1), "元", "www.baidu.com"))
                .andExpect(jsonPath("$.name").value("可乐"))
                .andExpect(jsonPath("$.price").value(1))
                .andExpect(jsonPath("$.unit").value("元"))
                .andExpect(jsonPath("$.url").value("www.baidu.com"));
    }

    @Test
    void should_return_success_when_get_all_product() throws Exception {
        getPerform(new CreateProductRequest("可乐1", new BigDecimal(1), "元", "www.baidu.com"));
        getPerform(new CreateProductRequest("可乐2", new BigDecimal(2), "元", "www.baidu.com"));
        mockMvc.perform(get("/api/products"))
                .andExpect(jsonPath("$[0].name").value("可乐1"))
                .andExpect(jsonPath("$[0].price").value(1))
                .andExpect(jsonPath("$[0].unit").value("元"))
                .andExpect(jsonPath("$[0].url").value("www.baidu.com"))
                .andExpect(jsonPath("$[1].name").value("可乐2"))
                .andExpect(jsonPath("$[1].price").value(2))
                .andExpect(jsonPath("$[1].unit").value("元"))
                .andExpect(jsonPath("$[1].url").value("www.baidu.com"))
                .andExpect(jsonPath("$.length()").value(2));

    }


}
