package com.twc.backend.web;

import com.twc.backend.ApiTestBase;
import com.twc.backend.request.CreateOrderRequest;
import com.twc.backend.request.CreateProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class OrderControllerTest extends ApiTestBase {


    @Test
    void should_return_success_when_create_order() throws Exception {
        getPerform(new CreateProductRequest("可乐", new BigDecimal(1), "元", "www.baidu.com"));
        getPerform(new CreateProductRequest("可乐1", new BigDecimal(2), "元", "www.baidu.com"));
        mockMvc.perform(post("/api/orders").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(mapper.writeValueAsBytes(new CreateOrderRequest(1L))))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.length()").value(2));

    }
}
